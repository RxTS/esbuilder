# @esbuilder/html

## 0.0.5

### Patch Changes

- [`539ed7b`](https://github.com/rx-ts/esbuilder/commit/539ed7b9961cb454a1f2f0d06f50d84c02fa7346) Thanks [@JounQin](https://github.com/JounQin)! - fix(peer): update peer esbuild version range

## 0.0.4

### Patch Changes

- [`0438212`](https://github.com/rx-ts/esbuilder/commit/04382129496eaa3906e3435513ec4c4b35fc0134) Thanks [@JounQin](https://github.com/JounQin)! - fix(types): add types field to exports

## 0.0.3

### Patch Changes

- [#6](https://github.com/rx-ts/esbuilder/pull/6) [`983b651`](https://github.com/rx-ts/esbuilder/commit/983b651c9507bf935c4ca320183a6db03c0e6e5f) Thanks [@JounQin](https://github.com/JounQin)! - fix: enable compatibility with sourcemap enabled

## 0.0.2

### Patch Changes

- [#4](https://github.com/rx-ts/esbuilder/pull/4) [`f05bbb2`](https://github.com/rx-ts/esbuilder/commit/f05bbb2d77ad6fcdd13394ea6022bb9fa79d7c49) Thanks [@JounQin](https://github.com/JounQin)! - fix: validate lang attr for script tag

## 0.0.1

### Patch Changes

- [#1](https://github.com/rx-ts/esbuilder/pull/1) [`61d81c4`](https://github.com/rx-ts/esbuilder/commit/61d81c4b7aab18ca384817db668b671a05968fd9) Thanks [@JounQin](https://github.com/JounQin)! - feat: first blood, should just work
