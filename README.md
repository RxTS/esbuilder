# esbuilder

[![GitHub Actions](https://github.com/rx-ts/esbuilder/workflows/CI/badge.svg)](https://github.com/rx-ts/esbuilder/actions/workflows/ci.yml)
[![Codacy Grade](https://img.shields.io/codacy/grade/41541a8ad8544f7d8df7b7df002f38c8)](https://www.codacy.com/gh/rx-ts/esbuilder)
[![Codecov](https://img.shields.io/codecov/c/gh/rx-ts/esbuilder)](https://codecov.io/gh/rx-ts/esbuilder)
[![type-coverage](https://img.shields.io/badge/dynamic/json.svg?label=type-coverage&prefix=%E2%89%A5&suffix=%&query=$.typeCoverage.atLeast&uri=https%3A%2F%2Fraw.githubusercontent.com%2Frx-ts%2Fesbuilder%2Fmain%2Fpackage.json)](https://github.com/plantain-00/type-coverage)
[![GitHub release](https://img.shields.io/github/release/rx-ts/esbuilder)](https://github.com/rx-ts/esbuilder/releases)
[![David Dev](https://img.shields.io/david/dev/rx-ts/esbuilder.svg)](https://david-dm.org/rx-ts/esbuilder?type=dev)

[![Conventional Commits](https://img.shields.io/badge/conventional%20commits-1.0.0-yellow.svg)](https://conventionalcommits.org)
[![Renovate enabled](https://img.shields.io/badge/renovate-enabled-brightgreen.svg)](https://renovatebot.com/)
[![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)
[![Code Style: Prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)
[![changesets](https://img.shields.io/badge/maintained%20with-changesets-176de3.svg)](https://github.com/atlassian/changesets)

> All in esbuild as primary bundler because it's extremely fast.
